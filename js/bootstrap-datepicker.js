var BootstrapDatepicker = function() {
    var t;
    t = mUtil.isRTL() ? {
        leftArrow: '<i class="la la-angle-right"></i>',
        rightArrow: '<i class="la la-angle-left"></i>'
    } : {
        leftArrow: '<i class="la la-angle-left"></i>',
        rightArrow: '<i class="la la-angle-right"></i>'
    };
    return {
        init: function() {
            $("#m_datepicker_1, #m_datepicker_1_validate").datepicker({
                rtl: mUtil.isRTL(),
                todayHighlight: !0,
                orientation: "bottom left",
                templates: t
            }), $("#m_datepicker_1_modal").datepicker({
                rtl: mUtil.isRTL(),
                todayHighlight: !0,
                orientation: "bottom left",
                templates: t
            }), $("#m_datepicker_2, #m_datepicker_2_validate").datepicker({
                rtl: mUtil.isRTL(),
                todayHighlight: !0,
                orientation: "bottom left",
                templates: t
            }), $("#m_datepicker_2_modal").datepicker({
                rtl: mUtil.isRTL(),
                todayHighlight: !0,
                orientation: "bottom left",
                templates: t
            })
        }
    }
}();
jQuery(document).ready(function() {
    BootstrapDatepicker.init()
});