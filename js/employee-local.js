var DatatableDataLocalDemo = {
    init: function() {
        var e, a;
        e = JSON.parse('[{"slno":1,"requestitle":"Annual Leave(Month)","submitdate":"04/07/2016","actiondate":"04/07/2016","actionwith":"Shahid Hamsa Hamsa Kidangassery Makkar Hamsa","duration":3,"status":1},{"slno":2,"requestitle":"Annual Leave(Month)","submitdate":"04/07/2016","actiondate":"04/07/2016","actionwith":"Shahid Hamsa Hamsa Kidangassery Makkar Hamsa","duration":5,"status":2},{"slno":3,"requestitle":"Annual Leave(Month)","submitdate":"04/07/2016","actiondate":"04/07/2016","actionwith":"Shahid Hamsa Hamsa Kidangassery Makkar Hamsa","duration":9,"status":3}]'),a = $(".m_datatable").mDatatable({
            data: {
                type: "local",
                source: e,
                pageSize:2
            },
            layout: {
                theme: "default",
                class: "",
                scroll: !1,
                footer: !1
            },
            sortable: !0,
            pagination: !0,
            search: {
                input: $("#generalSearch")
            },
            columns: [ {
                field: "slno",
                title: "Sl No",
				width:40
            },{
                field: "requestitle",
                title: "Request Title",
				width:119,
                responsive: {
                    visible: "lg"
                }
            },{
                field: "submitdate",
                title: "Submitted Date",
                type: "date",
                format: "MM/DD/YYYY",
				width:119
            },{
                field: "actiondate",
                title: "Action Date",
                type: "date",
                format: "MM/DD/YYYY",
				width:100
            }, {
                field: "actionwith",
                title: "Action With",
				width:130,
                responsive: {
                    visible: "lg"
                }
            }, {
                field: "duration",
                title: "Duration",
				width:60
            }, {
                field: "status",
                title: "Status",
                template: function(e) {
                    var a = {
                        1: {
                            title: "Pending",
                            class: "m-badge--danger"
                        },
                        2: {
                            title: "Approved",
                            class: "m-badge--success"
                        },
                        3: {
                            title: "Rejected",
                            class: " m-badge--danger"
                        }
                    };
                    return '<span class="m-badge ' + a[e.status].class + ' m-badge--wide">' + a[e.status].title + "</span>"
                }
            },  {
                field: "Actions",
                width: 110,
                title: "Actions",
                sortable: !1,
                overflow: "visible",
                template: function(e, a, i) {
                    return '\t\t\t\t\t\t<div class="dropdown ' + (i.getPageSize() - a <= 4 ? "dropup" : "") + '">\t\t\t\t\t\t\t<a href="#" class="btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="dropdown">                                <i class="la la-ellipsis-h"></i>                            </a>\t\t\t\t\t\t  \t<div class="dropdown-menu dropdown-menu-right">\t\t\t\t\t\t    \t<a class="dropdown-item" href="#"><i class="la la-edit"></i> Edit Details</a>\t\t\t\t\t\t    \t<a class="dropdown-item" href="#"><i class="la la-leaf"></i> Cancel Request</a>\t\t\t\t\t\t     \t<a class="dropdown-item" href="#"><i class="la la-leaf"></i>Reapply</a>\t\t\t\t\t\t    \t<a class="dropdown-item" href="../../../../default/leaveresumptionform.html"><i class="la la-print"></i> Leave Resumption Request</a>\t\t\t\t\t\t  \t</div>\t\t\t\t\t\t</div>\t\t\t\t\t\t<a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="View ">                            <i class="la la-edit"></i>                        </a>\t\t\t\t\t'
                }
            }]
        }), $("#m_form_status").on("change", function() {
            a.search($(this).val(), "status")
        }), $("#m_form_type").on("change", function() {
            a.search($(this).val(), "Type")
        }), $("#m_form_status, #m_form_type").selectpicker()
    }
};
jQuery(document).ready(function() {
    DatatableDataLocalDemo.init()
});